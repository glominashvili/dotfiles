# .bash_profile

# Get the aliases and functions
[ -f $HOME/.bashrc ] && . $HOME/.bashrc

export PATH="$PATH":"$HOME/.local/bin/"

export EDITOR="vim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export FILE="nnn"
export SUDO_ASKPASS="$HOME/.local/bin/dmpwd"
export COLORTERM="truecolor"

test -r "~/.dircolors" && eval $(dircolors ~/.dircolors)

exec startx
